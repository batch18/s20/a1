let courses = [
	
	{
		id: `Math101`,
		name: `Basic Mathemathics`,
		description: `introduction to math`,
		price: 500,
		isActive: true
	},
	{
		id: `Science101`,
		name: `Basic Science`,
		description: `Introduction to science`,
		price: 500,
		isActive: true
	},
	{
		id: `language101`,
		name: `Common Language`,
		description: `Introduction to common language`,
		price: 300,
		isActive: true
	},
	{
		id: `Alchemy101`,
		name: `Basic Alchemy`,
		description: `learn how to make potions`,
		price: 300,
		isActive: false
	}

];


const addCourses = (id, name, desc, price, isActive	) => {
	courses.push(
			{
				id: id,
				name: name,
				description: desc,
				price: price,
				isActive: isActive
			},
		);
	alert(`you have created ${id}. its price is ${price}`)
	return courses	
}

const deleteCourse = () => courses.pop();


const getSingleCourse = (id) => {
		let foundCourse = courses.find(function(course){
		return course.id === id;
	});
		console.log(foundCourse)
}

const getAllCourses = () => courses.map(function(element){
    return element;
});

const archiveCourse = (name, status) =>{
	const index = courses.findIndex((element, index) => {
	  if (element.name === name) {
	    return true
	  }
	})
	courses[index].isActive = status;
}

const seeActive = courses.filter(course => course.isActive === true);

